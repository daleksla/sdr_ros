# SDRROS
## README

SDRROS (Salih's Dead Reckoning for ROS) is a dead reckoning implementation which uses the distance travelled by a car and the heading angle it's been travelling at.
Programmed in the ROS framework for easy integration into other applications.

Takes in two formats:
  * 'speed info' - velocity, time, heading
  * 'distance info' - distance, heading

For the latter, the `ticks_to_distance` ROS service is available (ie. if encoder ticks / wheel pulses need to be translated).
