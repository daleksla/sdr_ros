#ifndef TRAVEL_INFO_HPP
#define TRAVEL_INFO_HPP
#pragma once

#include <sdros/distance_info.h> // message itself
#include <sdros/speed_info.h> // message itself

/**
  * @brief This file contains declarations & definitions of the callback and getters to obtain to movement_info & velocity_info ROS message
  * @author Salih Ahmed
**/

namespace sdros {

    class TravelInfo {
        /** @brief MovementInfo - class to store values returned by callback pertaining to the velocity+time/distance and the heading angle during said values occurance **/
        private:
            double distance_ ;

            double heading_ ;

        public:
            /** @brief TravelInfo - constructor initialises class values **/
            TravelInfo() ;

            /** @brief distance - inline method returning distance travelled by car
              * @return double - value pertaining to distance travelled **/
            inline double distance() const ;

            /** @brief heading - inline method returning angle car was heading at
              * @return double - value pertaining to heading angle travelled **/
            inline double heading() const ;

            /** @brief callback - (overload) inline method extracting distance travelled and heading angle from ROS message
              * @param const sdros::distance_infoConstPtr& - const reference to boost shared pointer to ROS message storing distance travelled and heading angle **/
            inline void callback(const sdros::distance_infoConstPtr&) ;

            /** @brief callback - (overload) inline method extracting velocity travelled at, heading angle and the duration of such travel from ROS message
              * @param const sdros::speed_infoConstPtr& - const reference to boost shared pointer to ROS message storing velocity travelled at, heading angle and the duration of such travel **/
            inline void callback(const sdros::speed_infoConstPtr&) ;

            /* DELETING NON NEEDED METHODS */
            TravelInfo(const TravelInfo&) = delete ;
            TravelInfo& operator=(const TravelInfo&) = delete ;
            TravelInfo(TravelInfo&&) = delete ;
            TravelInfo&& operator=(TravelInfo&&) = delete ;

            /** @brief ~TravelInfo - default destructor **/
            ~TravelInfo() = default ;
    } ;

}

sdros::TravelInfo::TravelInfo() : distance_(0), heading_(0) {} ;

inline double sdros::TravelInfo::distance() const
{
    return this->distance_ ;
}

inline double sdros::TravelInfo::heading() const
{
    return this->heading_ ;
}

inline void sdros::TravelInfo::callback(const sdros::distance_infoConstPtr& movement_info)
{
    this->distance_ = movement_info->distance ;
    this->heading_ = movement_info->heading ;
}

inline void sdros::TravelInfo::callback(const sdros::speed_infoConstPtr& velocity_info)
{
    this->distance_ = velocity_info->velocity * velocity_info->time ;
    this->heading_ = velocity_info->heading ;
}

#endif // TRAVEL_INFO_HPP
