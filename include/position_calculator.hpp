#ifndef POSITION_CALCULATOR_HPP
#define POSITION_CALCULATOR_HPP
#pragma once

#include <cmath>
#include <array>
#include <string>

#include <geometry_msgs/Pose.h>

/**
  * @brief This file contains functions to calculate new positions for SDROS
  * @author Salih Ahmed
**/

namespace sdros {

    /** @brief calculate_position_delta - function (overload) calculates change in position. SOH CAH TOA
      * @param const double& - const lvalue reference to value pertaining to distance travelled
      * @param const double& - const lvalue reference alue pertaining to angle distance was travelled at
      * @return std::tuple<double, double, double> - tuple storing three doubles pertaining to shift to position for each dimension (x, y, z) **/
    std::tuple<double, double, double> calculate_position_delta(const double&, const double&) ;

    /** @brief apply_position_delta - function applies the calculated change in position to old recorded position
      * @param geometry_msgs::Point& - reference to position storing variable (initially containing old position)
      * @param const double& - const lvalue refernece to x shift to add to the local position
      * @param const double& - const lvalue refernece to y shift to add to the local position
      * @param const double& - const lvalue refernece to z shift to add to the local position **/
    void apply_position_delta(geometry_msgs::Point&, const double&, const double&, const double&) ;

    /** @brief euler_to_quaternion - function applies the calculated change in position to old recorded position **/
    std::tuple<double, double, double, double> euler_to_quaternion(const double, const double, const double) ;

}

std::tuple<double, double, double> sdros::calculate_position_delta(const double& distance, const double& heading)
{
    const double delta_x = std::sin(heading) * distance ;

    const double delta_y = 0 ; // car doesn't move along y axis (ie not up or down)

    const double delta_z = std::cos(heading) * distance ;

    return {delta_x, delta_y, delta_z} ;
}

void sdros::apply_position_delta(geometry_msgs::Point& position, const double& delta_x, const double& delta_y, const double& delta_z)
{
    position.x += delta_x ;
    position.y += delta_y ;
    position.z += delta_z ;
}

std::tuple<double, double, double, double> sdros::euler_to_quaternion(const double yaw, const double pitch, const double roll)
{
    const double qx = std::sin(roll/2) * std::cos(pitch/2) * std::cos(yaw/2) - std::cos(roll/2) * std::sin(pitch/2) * std::sin(yaw/2) ;
    const double qy = std::cos(roll/2) * std::sin(pitch/2) * std::cos(yaw/2) + std::sin(roll/2) * std::cos(pitch/2) * std::sin(yaw/2) ;
    const double qz = std::cos(roll/2) * std::cos(pitch/2) * std::sin(yaw/2) - std::sin(roll/2) * std::sin(pitch/2) * std::cos(yaw/2) ;
    const double qw = std::cos(roll/2) * std::cos(pitch/2) * std::cos(yaw/2) + std::sin(roll/2) * std::sin(pitch/2) * std::sin(yaw/2) ;
    return {qx, qy, qz, qw} ;
}

#endif // POSITION_CALCULATOR_HPP
