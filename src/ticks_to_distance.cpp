// ROS stuff
#include <ros/ros.h>
#include <ros/console.h> // ROS_INFO_ERROR, ROS_ERROR_STREAM etc.

// standard CXX stuff
#include <string>
#include <cmath>

// this package's stuff
#include <sdros/ticks_to_distance.h> // callback to deal with the above movement_info message

/**
  * @brief This file acts as a server to convert (encoder) ticks to distance values, provided it's given the ticks done, the ticks per wheel and the wheel circumference
  * @author Salih Ahmed
**/

namespace sdros {

    bool convert_ticks_to_distance(sdros::ticks_to_distance::Request&, sdros::ticks_to_distance::Response&) ;

}

bool sdros::convert_ticks_to_distance(sdros::ticks_to_distance::Request& request, sdros::ticks_to_distance::Response& response)
{
    response.distance = request.ticks * (request.wheel_circumference / request.ticks_per_wheel) ;
    return true ;
}

int main(int argc, char** argv)
{
    const std::string node_name = "ticks_to_distance" ;
	ros::init(argc, argv, node_name); ROS_INFO_STREAM(node_name << " running") ; // initialise ROS node
	ros::NodeHandle nh("~") ; // handle to ROS communications (topics, services)

    ros::ServiceServer service = nh.advertiseService("ticks_to_distance", &sdros::convert_ticks_to_distance) ;
    ros::spin() ;

	/* E(nd) O(f) P(rogram) */
	return 0 ;
}
