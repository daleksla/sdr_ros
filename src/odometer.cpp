// ROS stuff
#include <ros/ros.h>
#include <ros/console.h> // ROS_INFO_ERROR, ROS_Eno matching function for call to 'stod(const double&)' RROR_STREAM etc.
#include <ros/callback_queue.h>

// standard CXX stuff
#include <string>
#include <cmath>

// what odometry information will be stored and pushed as
#include <geometry_msgs/Pose.h>

// this package's stuff
#include <travel_info.hpp> // callback to deal with the both movement_info and velocity_info message
#include <position_calculator.hpp> // function to deal with calculating position

/**
  * @brief This file contains the main functionality for the ROS-node 'odometer' as part of the SDROS package.
           Gets movement data and updates stored position and pushes it into the wider ROS network.
  * @author Salih Ahmed
**/

int main(int argc, char** argv)
{
    const std::string node_name = "odometer" ;
    ros::init(argc, argv, node_name); ROS_INFO_STREAM(node_name << " running") ; // initialise ROS node
    ros::NodeHandle nh("~") ; // handle to ROS communications (topics, services)

    /* ROS messages */
    sdros::TravelInfo cb ; // callback object with methods
    ros::Subscriber sub1 = nh.subscribe<sdros::speed_info>("/speed_info", 100, &sdros::TravelInfo::callback, &cb) ; // subscribe to topic speed_info
    ros::Subscriber sub2 = nh.subscribe<sdros::distance_info>("/distance_info", 100, &sdros::TravelInfo::callback, &cb) ; // subscribe to topic distance_info

    geometry_msgs::Pose point ;
    ros::Publisher pub = nh.advertise<geometry_msgs::Pose>("/odometry", 1) ; // publishes information about where car as XYZ coordinates

    /* Main (odometer) functionality */
    while(ros::ok()) // while ROS is running - the actual useful computation
    {
        /* Get info */
        if(ros::getGlobalCallbackQueue()->callOne() == ros::CallbackQueue::CallOneResult::Empty) continue ;
        // ^ we can't simply use `ros::spinOnce` as there's no indication of an empty queue, meaning we'd be processing the same message multiple times, expoinentially increasing predicted distance.

        const double distance = cb.distance() ; if(!distance) continue ; // no point processing odom when we haven't moved'
        const double heading = cb.heading() ;
        ROS_INFO_STREAM("Heading at " << heading << " degrees for " << distance << " meters") ;

        /* Calculate new coordinates */
        const auto [delta_x, delta_y, delta_z] = sdros::calculate_position_delta(distance, heading) ;
        ROS_INFO("Position shift: (x: %f) (y: %f) (z: %f)", delta_x, delta_y, delta_z) ;

        sdros::apply_position_delta(point.position, delta_x, delta_y, delta_z) ;
        ROS_INFO("Current position: (x: %f) (y: %f) (z: %f)", point.position.x, point.position.y, point.position.z) ;

        /* Calculate new orientation */


        /* Publish odometry */
        pub.publish(point) ;
    }

    /* E(nd) O(f) P(rogram) */
    return 0 ;
}
